/*
Плагин выполняет exec noct.cfg когда из КТ выходят все игроки. И ct.cfg когда за КТ заходит хоть 1 игрок.
Проверяет кол-во КТ каждый раз когда из КТ кто-то выходит/заходит, и каждые 5 секунд на всякий случай.

*/
#include <cstrike> 
public Plugin:myinfo =
{
 name = "no CT cfg",
 author = "ShaRen",
 description = "Меняет config когда нет КТ",
 version =  "1.0.0"
};

public OnPluginStart()
{
	HookEvent("player_team", eV_player_team);
	HookEvent("round_start", eV_RoundStart);
}



public Action:eV_RoundStart(Handle:event, const String:name[], bool:dontBroadcast)
	if (_CT())	// за КТ никого не было
	{
		ServerCommand("sm_open_jail");
	}

public Action:eV_player_team(Handle:event, const String:name[], bool:dontBroadcast) //Когда кто-то меняет команду
{
	new team = GetEventInt(event, "team");
	new oldteam = GetEventInt(event, "oldteam");
	if (team == 3)	//если заходят за КТ
	{
		if (_CT())	//и за КТ никого не было
		{
			ServerCommand("exec ct.cfg");
		}
	}
	if (oldteam == 3)		//если выходят из КТ
	{
		if (_1CT())	//за КТ был 1 или менье
		{
//			CS_TerminateRound(0.1, CSRoundEnd_Draw);
			ServerCommand("exec noct.cfg");
		}
	}
	if (team == 2)	//если заходят за Т
	{
		if (_CT() && _1T())	//за КТ никого не было, а за Т 1 или менее
		{
			ServerCommand("exec noct.cfg");  //т.к. в начале карты из КТ никто не выходил
		}
	}
	if (team == 2)	//если заходят за Т
	{
		if (_CT())	//за КТ никого не было
		{
			ServerCommand("sm_respawn @t");
		}
	}
}

bool:_CT()
{
	new t = 0;
	for (new i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && GetClientTeam(i) == 3 && ++t > 0) return false;		//за КТ кто-то есть
	}
	return true;		//за КТ пусто
}

bool:_1CT()
{
	new t = 0;
	for (new i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && GetClientTeam(i) == 3 && ++t > 1) return false;		//за КТ больше 1 (2 и больше)
	}
	return true;		//за КТ 1 или менье
}
bool:_1T()
{
	new t = 0;
	for (new i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i) && GetClientTeam(i) == 2 && ++t > 1) return false;		//за Т больше 1 (2 и больше)
	}
	return true;		//за Т 1 или менье
}